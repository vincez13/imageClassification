# -*- coding: utf-8 -*-

import cv2
import numpy as np

from abc import ABCMeta, abstractmethod
from matplotlib import pyplot as plt


class Classifier:

    __metaclass__ = ABCMeta

    @abstractmethod
    def fit(self, x_train, y_train):
        pass

    @abstractmethod
    def evaluate(self, x_test, y_test, visualize=False):
        pass

    def _accuracy(self, y_test, y_vote):
        y_predict = np.argmax(y_vote, axis=1)
        mask = y_predict == y_test
        return np.float32(np.count_nonzero(mask)) / len(y_test)

    def _precision(self, y_test, y_vote):
        y_predict = np.argmax(y_vote, axis=1)
        if self.mode == 'one-vs-one':
            conf = self._confusion(y_test, y_vote)
            precison = np.zeros(self.num_classes)
            for c in xrange(self.num_classes):
                true_positive = conf[c,c]
                false_positive = np.sum(conf[:, c]) - conf[c, c]
                if true_positive + false_positive != 0:
                    precison[c] = true_positive * 1.0 / (true_positive + false_positive)
        elif self.mode == "one-vs-all":
            precison = np.zeros(self.num_classes)
            for c in xrange(self.num_classes):
                true_positive = np.count_nonzero((y_test == c) * (y_predict == c))
                false_positive = np.count_nonzero((y_test == c) *(y_predict != c))
                if true_positive + false_positive != 0:
                    precison[c] = true_positive * 1.0 / (true_positive + false_positive)
        return precison

    def _recall(self, y_test, y_vote):
        y_predict = np.argmax(y_vote, axis=1)
        if self.mode == 'one-vs-one':
            conf = self._confusion(y_test, y_vote)
            recall = np.zeros(self.num_classes)
            for c in xrange(self.num_classes):
                true_positive = conf[c, c]
                false_negative = np.sum(conf[c, :]) - conf[c, c]
                if true_positive + false_negative != 0:
                    recall[c] = true_positive * 1.0 / (true_positive + false_negative)
        elif self.mode == "one-vs-all":
            recall = np.zeros(self.num_classes)
            for c in xrange(self.num_classes):
                true_positive = np.count_nonzero((y_test == c) * (y_predict == c))
                false_negative = np.count_nonzero((y_test != c) * (y_predict == c))
                if true_positive + false_negative != 0:
                    recall[c] = true_positive * 1.0 / (true_positive + false_negative)
        return recall

    def _confusion(self, y_test, y_vote):
        y_predict = np.argmax(y_vote, axis=1)
        conf = np.zeros((self.num_classes, self.num_classes)).astype(np.int32)
        for c_true in xrange(self.num_classes):
            for c_predict in xrange(self.num_classes):
                y_this = np.where((y_test == c_true) * (y_predict == c_predict))
                conf[c_predict, c_true] = np.count_nonzero(y_this)
        return conf

class MultiClassSVM(Classifier):

    def __init__(self, num_classes, mode="one-vs-one", params=None):

        self.num_classes = num_classes
        self.mode = mode
        self.params = params or dict()

        self.classifiers = []
        if mode == "one-vs-one":
            for _ in xrange(num_classes*(num_classes - 1) / 2):
                self.classifiers.append(cv2.SVM())
        elif mode == "one-vs-all":
            for _ in xrange(num_classes):
                self.classifiers.append(cv2.SVM())
        else:
            print "Unknown mode ", mode

    def fit(self, x_train, y_train, params=None):
        if params is None:
            params = self.params

        if self.mode == "one-vs-one":
            svm_id = 0
            for c1 in xrange(self.num_classes):
                for c2 in xrange(c1 + 1, self.num_classes):
                    # print "Comparing ", c1 , " with " , c2
                    data_id = np.where((y_train == c1) + (y_train == c2))[0]
                    x_train_id = x_train[data_id, :]
                    y_train_id = y_train[data_id]
                    y_train_bin = np.where(y_train_id == c1, 1, 0).flatten()
                    self.classifiers[svm_id].train(x_train_id, y_train_bin, params=self.params)
                    svm_id += 1

        elif self.mode == "one-vs-all":
            for c in xrange(self.num_classes):

                y_train_bin = np.where(y_train == c, 1, 0).flatten()
                self.classifiers[c].train(x_train, y_train_bin, params=self.params)

    def evaluate(self, x_test, y_test, visualize=False, result_file='text1.txt', vote_file='vote1.txt'):

        y_vote2 = np.zeros((len(y_test), self.num_classes))
        if self.mode == "one-vs-one":
            svm_id = 0
            for c1 in xrange(self.num_classes):
                for c2 in xrange(c1+1, self.num_classes):

                    y_predict2 = self.classifiers[svm_id].predict_all(x_test)


                    for i in xrange(len(y_predict2)):
                        if y_predict2[i] == 1:
                            y_vote2[i, c1] += 1
                        elif y_predict2[i] == 0:
                            y_vote2[i, c2] += 1
                        else:
                            print "y_predict2[", i, "] = ", y_predict2[i]

                    svm_id += 1
        elif self.mode == "one-vs-all":
            for c in xrange(self.num_classes):
                y_predict = self.classifiers[c].predict_all(x_test)
                if np.any(y_predict):
                    y_vote2[np.where(y_predict == 1)[0], c] += 1
            no_label = np.where(np.sum(y_vote, axis=1) == 0)[0]
            y_vote2[no_label, np.random.randint(self.num_classes, size=len(no_label))] = 1


        result = np.zeros((self.num_classes, 3))

        y_predict = np.argmax(y_vote2, axis=1)

        y_vote_result = np.zeros((len(y_predict), self.num_classes))

        for i in xrange(len(y_predict)):
            y_vote_result[i][y_predict[i]] = 1


        for i in xrange(len(y_test)):
            result[int(y_test[i])][0] += 1
            if y_test[i] == y_predict[i]:
                result[int(y_predict[i])][1] += 1
        for i in xrange(len(result)):
            result[i][2] = 100.0 * result[i][1] / result[i][0]
        np.savetxt(vote_file, y_vote_result, fmt='%3d')
        np.savetxt(result_file, result, fmt='%3d')
        return result
