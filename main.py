# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import pyplot as plt
import time
import os

import loader
from classifiers import MultiClassSVM

def main():
    start = time.time()
    strategy = 'one-vs-one'
    # strategy = 'one-vs-all'
    feature = 'hog'

    seed = np.random.randint(1,500)
    rootpath ="./101_ObjectCategories/"
    dirlist = os.listdir(rootpath)
    range_start = 0
    range_end = len(dirlist)
    range_step = 1
    test_split = 0.1

    tests_avg = 0

    for i in xrange(1,11):
        print "Test: ", i
        test_time = time.time()
        (x_train, y_train), (x_test, y_test) = loader.load_data(rootpath=rootpath, feature=feature, test_split=test_split, seed=seed, iteration=i, range_start=range_start, range_end=range_end, range_step=range_step)
        loaded = time.time()
        print "Features extraced in ", (loaded - test_time)


        x_train = np.squeeze(np.array(x_train)).astype(np.float32)

        y_train = np.array(y_train)
        x_test = np.squeeze(np.array(x_test)).astype(np.float32)

        y_test = np.array(y_test)

        labels = np.unique(np.hstack((y_train, y_test)))

        MCS = MultiClassSVM(len(labels), strategy)
        print " - training - "
        MCS.fit(x_train, y_train)
        trained = time.time()
        print "Data trained in ", (trained - loaded)

        print " - evaluating - "
        result_file = "./results/result" + str(i) + ".txt"
        vote_file = "./results/vote" + str(i) + ".txt"

        result = MCS.evaluate(x_test, y_test, result_file=result_file, vote_file=vote_file)
        summ = 0
        for i in xrange(len(result)):
            summ += result[i][2]
        avg = np.float32(np.sum(summ))/len(result)
        tests_avg += avg
        print avg
        print
        tested = time.time()
        print "Total time spent: ", (tested - test_time)

    tests_avg /= 10
    print "Test avg: ", tests_avg

if __name__ == '__main__':
    main()
