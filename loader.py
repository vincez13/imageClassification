# -*- coding: utf-8 -*-

import cv2
import numpy as np
import os

from matplotlib import cm
from matplotlib import pyplot as plt

from sklearn.decomposition import PCA

def load_data(rootpath="./101_ObjectCategories/", feature='hog', test_split=0.1, seed=69, range_start=0, range_end=102, range_step=1, iteration=1):
    dirlist = os.listdir(rootpath)
    classes = np.arange(range_start, range_end, range_step)
    data = {}
    for c in xrange(len(classes)):
        dirpath = rootpath + dirlist[classes[c]] + '/'
        filelist = os.listdir(dirpath)

        data[c] = []
        for filename in filelist:
            path = dirpath + filename
            im = cv2.imread(path)
            data[c].append(im)

    data = _extract_feature(data, feature)

    x_train = []
    y_train = []

    x_test = []
    y_test = []


    for ii in xrange(len(data)):
        np.random.seed(seed)
        np.random.shuffle(data[ii])
        x_train_temp = (data[ii][:int(len(data[ii])*(1 - test_split*iteration))] + data[ii][int(len(data[ii])*(1 - test_split*(iteration-1))):])
        x_train += x_train_temp
        y_train += [ii for jj in xrange(len(x_train_temp))]

        x_test_temp = (data[ii][int(len(data[ii])*(1 - test_split*iteration)):int(len(data[ii])*(1 - test_split*(iteration-1)))])
        x_test += x_test_temp
        y_test += [ii for jj in xrange(len(x_test_temp))]

    return (x_train, y_train), (x_test, y_test)

def _extract_feature(data, feature):


    if feature == 'surf': # not work
        surf = cv2.SURF(1000)
        surf.upright = True
        surf.extended = True
        num_surf_features = 50
        winSize = (150,150)
        dense = cv2.FeatureDetector_create("Dense")
        kp = dense.detect(np.zeros(winSize))


        for ii in xrange(len(data)):
            desList = []
            for image in data[ii]:
                des = surf.compute(image, kp)

                des = des[1][:num_surf_features, :]
                desList += [des]

            data[ii] = desList

    elif feature == 'hog':
        winSize = (32, 32)
        winResize = (32, 32)
        for ii in xrange(len(data)):
            data[ii] = [cv2.resize(image, winResize) for image in data[ii]]

        blockSize = (16, 16)
        blockStride = (8, 8)
        cellSize = (8, 8)
        nbins = 9
        hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins)

        bb = False

        for ii in xrange(len(data)):
            ll = []
            for image in data[ii]:
                test = hog.compute(image)

                ll.append(test)

                if not bb:
                    print len(test)
                    bb = True

            data[ii] = ll


    return data
